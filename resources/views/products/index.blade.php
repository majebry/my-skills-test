@extends('layouts.master')

@section('content')
    <div class="row">
        <h2>Submitting a product:</h2>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Product Form</h3>
            </div>
            <div class="panel-body">

                <form action="" method="post" id="product_form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="product_name">Product name</label>
                        <input name="product_name" type="text" class="form-control" id="product_name" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="quantity_in_stock">Quantity in stock</label>
                        <input name="quantity_in_stock" type="number" class="form-control" id="quantity_in_stock" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="price_per_item">Price per item</label>
                        <input name="price_per_item" type="number" id="price_per_item" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            </div>
        </div>
    </div>

    @if (count($products))
    <div class="row">
        <h2>Last Products:</h2>

        <table class="table">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Quantity in Stock</th>
                    <th>Price per Item</th>
                    <th>Created at</th>
                    <th>Total value</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->product_name }}</td>
                        <td>{{ $product->quantity_in_stock }}</td>
                        <td>{{ $product->price_per_item }}</td>
                        <td>{{ $product->created_at->date }}</td>
                        <td>{{ $product->total_value }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>{{ $sum_total_values }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    @else
        <div class="alert alert-info">
            <p>Please refresh the page to see if there's new products :(</p>
        </div>
    @endif

@endsection

@section('script')
    <script type="text/javascript">
    $( document ).ready(function() {
        $('#product_form').on('submit', function(e) {
            e.preventDefault();
            var product_name = $('#product_name').val();
            var quantity_in_stock = $('#quantity_in_stock').val();
            var price_per_item = $('#price_per_item').val();
            $.ajax({
                type: "POST",
                url: 'products',
                data: {product_name:product_name, quantity_in_stock:quantity_in_stock, price_per_item:price_per_item, _token: '{{csrf_token()}}'},
                success: function( data ) {
                    console.log( data );
                }
            });
        });
    });
    </script>
@endsection
