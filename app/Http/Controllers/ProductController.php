<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Carbon\Carbon;

class ProductController extends Controller
{
    public function index()
    {
        $files = Storage::disk('local')->files('products');

        $products = [];

        $sum_total_values = 0;

        if (count($files)) {
            foreach ($files as $file) {

                if (substr($file, -5) == '.json') {
                    $file_path = Storage::disk('local')->get($file);
                    $product = json_decode($file_path);
                    // $product->created_at = Carbon::parse($product->created_at)->toDateString();
                    $product->total_value = $product->quantity_in_stock * $product->price_per_item;

                    $products[] = $product;

                    $sum_total_values += $product->total_value;
                }
            }
        }

        return view('products.index', compact('products', 'sum_total_values'));
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            $new_product = [];
            $new_product = $request->only([
                'product_name',
                'quantity_in_stock',
                'price_per_item'
            ]);

            $new_product['created_at'] = Carbon::now();

            $json_product = json_encode($new_product);

            $new_file_name = time() . '.json';

            Storage::put('products/' . $new_file_name, $json_product);

            return response()->json(['success' => true]);
        }
    }
}
